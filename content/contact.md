+++
title = "Contact"
weight = 50
draft = false
+++
<a class="icon fa-envelope"><span class="label">Email</span></a> <luke@reegermachine.com>

<a class="icon fa-phone"><span class="label">Phone</span></a> [724-599-8792](tel:+1724-599-8792)

<a class="icon fa-building"><span class="label">Address</span>628 Dogwood Rd, Indiana PA 15701</a>



<form id="contactform" method="post" action="https://formspree.io/info@reegermachine.com">
	<div class="field half first">
		<input type="text" name="name" id="name" placeholder="Name"/>
	</div>
	<div class="field half">
		<input type="email" id="email" name="email" placeholder="Email">
	</div>
	<div class="field">
		<textarea name="message" id="message" rows="4" placeholder="Message"></textarea>
	</div>
	<ul class="actions">
		<li><input type="submit" value="Send message" class="special" /></li>
		<li><input type="reset" value="Reset" /></li>
	</ul>
	<input type="hidden" name="_next" value="?sent#formspree" />
	<input type="hidden" name="_subject" value="Subject for your mail like new message" />
	<input type="text" name="_gotcha" style="display:none" />
</form>
<span id="contactformsent">Thank you for your message</span>

<script>
$(document).ready(function($) {
    $(function(){
        if (window.location.search == "?sent") {
        	$('#contactform').hide();
        	$('#contactformsent').show();
        } else {
        	$('#contactformsent').hide();
        }
    });
});
</script>

{{< gmaps pb="!1m18!1m12!1m3!1d274206.00134074857!2d-79.34341815505766!3d40.59509321269438!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89cb5ad883eeb52f%3A0x65ff223f0053a21f!2sIndiana%2C+PA!5e0!3m2!1sen!2sus!4v1527204783133">}}

{{< socialLinks >}}
